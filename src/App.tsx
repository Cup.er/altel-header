import React from 'react';
import './App.css';
import {Header} from "./components/organisms/header/component";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import {Business} from "./components/pages/business/component";
import {Home} from "./components/pages/home/component";

function App() {
  return (
      <BrowserRouter>
          <div className="App">
              <Header />
              <div className="container-fluid">
                  <Switch>
                      <Route component={Business} path="/business" />
                      <Route component={Home} path="/home" />
                  </Switch>
              </div>
              <div className="footer">footer</div>
          </div>
      </BrowserRouter>

  );
}

export default App;
