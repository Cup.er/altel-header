import React, {useState} from "react";
import h from "./styles.module.css";
import {Link} from "react-router-dom";

export const Header: React.FC = () => {
    const headerTop_list = [
        {
            id: 1,
            title: "Бизнесу",
            checked: false,
            path: "business",
        },
        {
            id: 2,
            title: "Частным лицам",
            checked: true,
            path: "home",
        }
    ]
    const headerBottom_list = [
        {
            id: 1,
            title: "Тарифы",
            isOpen: false,
            options : [
                {
                    title: "Option 1"
                },
                {
                    title: "Option 2"
                },
                {
                    title: "Option 3"
                },
            ]
        },
        {
            id: 2,
            title: "Услуги",
            isOpen: false,
            options : [
                {
                    title: "Option 1"
                },
                {
                    title: "Option 2"
                },
                {
                    title: "Option 3"
                },
            ]
        },
        {
            id: 3,
            title: "Устройства",
            isOpen: false,
            options : [
                {
                    title: "Option 1"
                },
                {
                    title: "Option 2"
                },
                {
                    title: "Option 3"
                },
            ]
        },
        {
            id: 4,
            title: "Технологии",
            isOpen: false,
            options : [
                {
                    title: "Option 1"
                },
                {
                    title: "Option 2"
                },
                {
                    title: "Option 3"
                },
            ]
        },
        {
            id: 5,
            title: "Переводы и платежи",
            isOpen: false,
            options : [
                {
                    title: "Option 1"
                },
                {
                    title: "Option 2"
                },
                {
                    title: "Option 3"
                },
            ]
        },
    ]
    const languages = [
        {
            id: 1,
            title: "Рус",
            checked: false,
        },
        {
            id: 2,
            title: "Қаз",
            checked: true,
        }
    ]

    const [topList, setTopList] = useState(headerTop_list);
    const [list, setList] = useState(headerBottom_list);
    const [language, setLanguage] = useState(languages);

    const changeLanguage = (id: number) => {
        setLanguage(prev => prev.map(lang => {
            for (let i = 0; i < prev.length; i++) {
                prev[i].checked = false
            }
            if(lang.id === id){
                return {
                    ...lang,
                    checked: true
                }
            }
            return lang

        }))
    }


    const changePage = (id: number) => {
        setTopList(prev => prev.map(option => {
            for (let i = 0; i < prev.length; i++) {
                prev[i].checked = false
            }
            if(option.id === id){
                return {
                    ...option,
                    checked: true
                }
            }
            return option

        }))
    }

    const openDropdown = (id: number) => {
        setList(prev => prev.map(option => {
            if(option.id === id){
                return {
                    ...option,
                    isOpen: !option.isOpen
                }
            }
            return option
        }))
    }

    return (
        <div className={h.header}>
            <div className={h.header_top}>
                <div className={h.header_topCont+" container"}>
                    <div className={h.list_left}>
                        <ul>
                            {topList.map(list => <Link to={list.path}><li  onClick={changePage.bind(null, list.id)} key={`top-${list.id}`} className={list.checked ? h.checked : "" }>{list.title}</li></Link>)}
                        </ul>
                        <span className={h.divider} />
                        <div className={h.location}>
                        <span>
                            <img src="https://altel.kz/uploads/2020/7/27/875a6aedc212f468c3d24d9534e3f334_original.778.png" alt=""/>
                            Офисы и карта покрытия
                        </span>
                        </div>
                    </div>
                    <div className={h.list_right}>
                        <div className={h.languages}>
                            {language.map(lang =>
                                <div onClick={changeLanguage.bind(null, lang.id)} key={`lang-${lang.id}`} className={lang.checked ? h.lang+" "+h.choosen : h.lang}>
                                    {lang.title}
                                </div>)}

                            <span className={h.divider} />
                        </div>
                    </div>
                </div>
            </div>
            <div className={h.header_bottom}>
                <div className={h.header_bottomCont+" container"}>
                    <div className={h.logo}>
                        <img src="https://altel.kz/uploads/2021/3/11/25f277d9ebd8b28a8cea5fc65be4a2bb_original.2034.png" alt=""/>
                    </div>
                    <ul>
                        {list.map(li =>
                            <div>
                                <li onClick={openDropdown.bind(null, li.id)} key={`bottom-${li.id}`}>{li.title}</li>
                                <div className={h["dropdown-options"]} style={{display : li.isOpen ? 'block' : 'none'}}>
                                    {li.options.map(o => (
                                        <ul>
                                            <li>{o.title}</li>
                                        </ul>
                                    ))}
                                </div>
                            </div>
                        )}

                    </ul>
                    <div className={h.buttons}>
                        <button>Стать абонентом</button>
                        <button>
                            <svg xmlns="http://www.w3.org/2000/svg" width="9" height="10" viewBox="0 0 18 20" fill-rule="nonzero" clip-rule="evenodd" className="mr-2 css-0">
                                <path d="M8.99955 15C12.6616 15 15.8646 16.575 17.6066 18.925L15.7646 19.796C14.3466 18.116 11.8466 17 8.99955 17C6.15255 17 3.65255 18.116 2.23455 19.796L0.393555 18.924C2.13555 16.574 5.33755 15 8.99955 15ZM8.99955 0C10.3256 0 11.5974 0.526784 12.5351 1.46447C13.4728 2.40215 13.9996 3.67392 13.9996 5V8C13.9996 9.32608 13.4728 10.5979 12.5351 11.5355C11.5974 12.4732 10.3256 13 8.99955 13C7.67347 13 6.4017 12.4732 5.46402 11.5355C4.52634 10.5979 3.99955 9.32608 3.99955 8V5C3.99955 3.67392 4.52634 2.40215 5.46402 1.46447C6.4017 0.526784 7.67347 0 8.99955 0Z"></path>
                            </svg>
                            Личный кабинет
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}